# Tasksel Portuguese translation for the Debian Installer
# 2004 Miguel Figueiredo <elmig@debianPT.org>
# PT Translation Team <http://www.debianPT.org/traduz/>
#
# 2005-2020 - Miguel Figueiredo <elmig@debianpt.org>
#
msgid ""
msgstr ""
"Project-Id-Version: tasksel\n"
"Report-Msgid-Bugs-To: tasksel@packages.debian.org\n"
"POT-Creation-Date: 2018-05-23 01:37+0200\n"
"PO-Revision-Date: 2020-10-21 22:00+0100\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese Translation Team <traduz@debianpt.org>\n"
"Language: Portuguese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Choose software to install:"
msgstr "Escolha o software a instalar:"

#. Type: multiselect
#. Description
#: ../templates:1001
msgid ""
"At the moment, only the core of the system is installed. To tune the system "
"to your needs, you can choose to install one or more of the following "
"predefined collections of software."
msgstr ""
"Neste momento, apenas está instalado o coração do sistema. Para afinar o "
"sistema de acordo com as suas necessidades, pode escolher instalar uma ou "
"mais das seguintes colecções de software pré-definidas."

#. Type: multiselect
#. Description
#: ../templates:2001
msgid ""
"You can choose to install one or more of the following predefined "
"collections of software."
msgstr ""
"Pode escolher instalar uma ou mais das seguintes colecções de software pré-"
"definidas."

#. Type: multiselect
#. Description
#: ../templates:3001
msgid "This can be preseeded to override the default desktop."
msgstr "Pode ser feito preseed para ultrapassar o desktop predefinido."

#. Type: title
#. Description
#: ../templates:4001
msgid "Software selection"
msgstr "Selecção de software"

#~ msgid "${ORIGCHOICES}"
#~ msgstr "${CHOICES}"

#~ msgid "${CHOICES}, manual package selection"
#~ msgstr "${CHOICES}, selecção de pacotes manual"
